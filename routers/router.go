package routers

import (
  "bitbucket.org/edsel_mabilangan/nf-rest/controllers"
  "bitbucket.org/edsel_mabilangan/nf-rest/models"
  "github.com/astaxie/beego/orm"
  _ "github.com/go-sql-driver/mysql"
  "github.com/astaxie/beego"
  "fmt"
)

func init() {
  orm.RegisterDriver("mysql", orm.DR_MySQL)
    
  dbConf := models.Get_DSN("prod",false)
  orm.RegisterDataBase("default", "mysql", fmt.Sprintf(beego.AppConfig.String("dsn"),
    dbConf.PBXTRA.Get("user"), 
    dbConf.PBXTRA.Get("pass"), 
    dbConf.PBXTRA.Get("host"), 
    dbConf.PBXTRA.Get("port")))

  dbConf = models.Get_DSN("prod",true)
  orm.RegisterDataBase("opposite", "mysql", fmt.Sprintf(beego.AppConfig.String("dsn"),
    dbConf.PBXTRA.Get("user"), 
    dbConf.PBXTRA.Get("pass"), 
    dbConf.PBXTRA.Get("host"), 
    dbConf.PBXTRA.Get("port")))
      
  orm.RegisterModel(new(controllers.User), new(controllers.Cookies))
  if beego.AppConfig.String("runmode") == "dev" {
    orm.Debug = true
  }
    
  beego.Router("/", &controllers.MainController{})
  beego.Router("/login", &controllers.LoginController{})
  beego.Router("/logout", &controllers.LogoutController{})
  beego.Router("/ast/action/?:action", &controllers.AstActController{})
}
