package models

import (
  "fmt"
  "os"
  "reflect"
  "regexp"
)

type dbSetting struct {
  host  string
  port  string
  user  string
  pass  string
}

func (s *dbSetting) Get(attr string) string {
  _, ok := reflect.TypeOf(s).Elem().FieldByName(attr)
  
  if !ok {
    return ""
  }
  
  r := reflect.ValueOf(s)
  f := reflect.Indirect(r).FieldByName(attr)
  
  return fmt.Sprintf("%v",f)
}    

type DSN struct {
  PBXTRA    *dbSetting
  CONFIG    *dbSetting
  RECORDING *dbSetting
  MYDNS     *dbSetting
  VMX       *dbSetting
  MISC      *dbSetting
}

func Get_DSN(mode string, opposite bool) *DSN {
  var a = new(DSN)
  var is_trixbox = false
  
  hostname, _ := os.Hostname()
  r := regexp.MustCompile(`^pbl|^store|^tb`)
  if r.MatchString(hostname) {
    is_trixbox = true
  }
  
  switch mode {
  case "dev":
    {
      var server = "web-dev2"

      if is_trixbox && !opposite {
        server = "127.0.0.1"
      }

      var dev = &dbSetting{
        host: server,
        port: "3306",
        user: "fonality",
        pass: "iNOcallU",
      }  
      
      a.PBXTRA    = dev
      a.CONFIG    = dev
      a.RECORDING = dev
      a.MYDNS     = &dbSetting{
        host: server,
        port: "3306",
        user: "mydns",
        pass: "dns4allus3rs",
      }
      a.VMX       = dev
      a.MISC      = dev    
    }
  case "mock":
    {
      var mock = new(dbSetting)

      a.PBXTRA    = mock
      a.CONFIG    = mock
      a.RECORDING = mock
      a.MYDNS     = mock
      a.VMX       = mock
      a.MISC      = mock 
    }
  default:
    {
      var server = "db1"

      if is_trixbox && !opposite {
        server = "tbp-db1"
      }

      a.PBXTRA    = &dbSetting{
        host: server,
        port: "3306",
        user: "fonality",
        pass: "iNOcallU",
      }
      a.CONFIG    = &dbSetting{
        host: server,
        port: "3306",
        user: "fonality",
        pass: "iNOcallU",
      }
      a.RECORDING = &dbSetting{
        host: "cdr-db",
        port: "3306",
        user: "recordall",
        pass: "rick, your socks don't match",
      }
      a.MYDNS     = &dbSetting{
        host: "ns1",
        port: "3306",
        user: "mydns",
        pass: "dns4allus3rs",
      }
      a.VMX       = &dbSetting{
        host: server,
        port: "3306",
        user: "vmx",
        pass: "iNOcallU",
      }
      a.MISC      = &dbSetting{
        host: "misc-db1",
        port: "3306",
        user: "fonality",
        pass: "KdxWPT6VH6Wu9KXm",
      }   
    }    
  }
  
  return a
}


func Determine_Run_Mode(database string) string {
  // If we have a global run mode then let's use it
  if run_mode := os.Getenv("GLOBAL_DB_RUN_MODE"); run_mode != "" {
    return run_mode
  }
  
  // If we have a run mode for this database then let's use it.
  if run_mode := os.Getenv(fmt.Sprintf("%s_DB_RUN_MODE",database)); run_mode != "" {
    return run_mode
  }
  
  // Are we on a dev machine then make the run_mode 'dev'
  // Else set the run mode to production when we are _not_ on a cp development server.
  hostname, _ := os.Hostname()
  r := regexp.MustCompile(`(web11|web-dev2|tbp-dev1)`)
  if r.MatchString(hostname) {
    return "dev"
  } else {
    return "prod"
  }
}