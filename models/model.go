package models

import (
  "os"
  "regexp"
  "strings"
  "crypto/sha1"
  "encoding/hex"
)

func Is_Trixbox() bool {
  hostname, _ := os.Hostname()
  r := regexp.MustCompile(`^pbl|^store|^tb`)
  if r.MatchString(hostname) {
    return true
  }
  return false
}

func Encrypt_Pass(pass string, salt ...string) string {
  var pepper string
  
  if len(salt) > 0 {
    pepper = strings.Join([]string{salt[0], pass}, "")
  } else {
	pepper = strings.Join([]string{"rickiscool", pass}, "")
  }
  
  // SHA1 hash
  hash := sha1.New()
  hash.Write([]byte(pepper))
  hashBytes := hash.Sum(nil)

  // Hexadecimal conversion
  hexSha1 := hex.EncodeToString(hashBytes)
  
  return hexSha1
}  