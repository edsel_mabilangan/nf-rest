package controllers

type User struct {
  User_id              int     `orm:"auto"`
  Extension            string  `orm:"size(255)"`
  Username             string  `orm:"size(255)" valid:"Required"`
  Password             string  `orm:"size(40)" valid:"Required"`
  FirstName            string  `orm:"size(255)"`
  LastName             string  `orm:"size(255)"` 
  Type                 string  `orm:"size(30)"` 
  Language             string  `orm:"size(20)"`
  EmployeeEmail        string  `orm:"size(99)"`
  EmployeeIm           string  `orm:"size(99)"` 
  EmployeePhonenumber  string  `orm:"size(40)"`
  HumorRollover        int
  DefaultServer_id     int
  FeatureComponents    string  `orm:"size(255)"`
  HudPassword          string  `orm:"size(40)"`
  FoncallPassword      string  `orm:"size(40)"`
}

type Cookies struct {
  CookieValue          string  `orm:"pk;size(25)"`
  ServerId             int
  LoginType            string  `orm:"size(10)"`
  Mailbox              int
  CpUsername           string  `orm:"size(128)"`
  CpuExtension         string  `orm:"size(10)"`
  ServerIdList         string  `orm:"size(100)"`
  Ip                   string  `orm:"size(50)"`
  HelpRollover         int
  Language             int
  HumorRollover        int
  IsReseller           int
  FormToken            string  `orm:"size(25)"`
  TmpPassChange        int
}