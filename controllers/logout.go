package controllers

import (
  "github.com/astaxie/beego/orm"
  "github.com/astaxie/beego"
  "fmt"
)

type LogoutController struct {
  beego.Controller
}

func (this *LogoutController) Post() {
  var data = make(map[string]string)
  data["result"] = "Failure"
  data["Message"] = "You have to login first"
  
  token := this.Input().Get("token")
  secret := this.Input().Get("secret")
  
  if token != "" {
    this.Data["db_alias"] = this.GetSession("db_alias")
 
    o := orm.NewOrm()
    if this.Data["db_alias"] != nil {
      o.Using(this.Data["db_alias"].(string))
    }
  
    qs := o.QueryTable("cookies").Filter("FormToken", token).Filter("CookieValue", secret)
    if exist := qs.Exist(); exist {
      if num, err := qs.Delete(); err == nil && num > 0 {
        data["result"] = "Success"
        data["Message"] = "User logged out"

        this.DestroySession()
      } else {
        data["Message"] = fmt.Sprintf("ERROR: %s", err.Error())
      }    
    }
  }
  
  this.Data["json"] = &data 
  this.ServeJson()
}
