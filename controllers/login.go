package controllers

import (
  "bitbucket.org/edsel_mabilangan/nf-rest/models"
  "github.com/astaxie/beego/orm"
  "github.com/astaxie/beego"
  "unicode/utf8"
  "strings"
  "strconv"
  "time"
  "fmt"
)

type LoginController struct {
  beego.Controller
}

func (this *LoginController) Post() {
  // Destroy any session 
  this.DestroySession()
  this.Data["db_alias"] = "default"
  
  o := orm.NewOrm()  
  data := this.Authenticate(o)
  
  this.Data["json"] = &data
  this.ServeJson()
}

func (this *LoginController) Authenticate(o orm.Ormer) map[string]string {
  var data = make(map[string]string)
  data["result"] = "Failure"
  data["Message"] = "Invalid username or password"
  
  if this.Input().Get("username") == "" || this.Input().Get("password") == "" {
    return data
  }    
  
  o.Using(this.Data["db_alias"].(string))

  var user User
  err := o.QueryTable("user").Filter("Username", this.Input().Get("username")).Filter("FeatureComponents__isnull", false).Filter("Type", "cpu").One(&user)
  if err == orm.ErrMultiRows {
    data["Message"] = "Returned Multi Rows Not One"
  } else if err == orm.ErrNoRows {
    if this.Data["db_alias"] != "opposite" {
      this.Data["db_alias"] = "opposite"
      data = this.Authenticate(o)
    }  
  } else {
    if is_valid_credentials(user.Password, this.Input().Get("password")) {
      data["result"] = "Success"
      data["Message"] = "Login Successfully..."
      
      ext, _ := strconv.Atoi(user.Extension)
      
      cookie := Cookies{
        CookieValue   : models.RandStringBytesMaskImprSrc(16),
        ServerId      : user.DefaultServer_id,
        LoginType     : user.Type,
        Mailbox       : ext,
        CpUsername    : user.Username,
        CpuExtension  : user.Extension,
        ServerIdList  : strconv.Itoa(user.DefaultServer_id),
        Ip            : strings.Split(this.Ctx.Request.RemoteAddr,":")[0],
        HelpRollover  : 1,
        HumorRollover : user.HumorRollover,
        IsReseller    : 0,
        FormToken     : fmt.Sprintf("FO_%s%s_N", strconv.Itoa(user.DefaultServer_id), strconv.Itoa(int(time.Now().Unix()))),
        TmpPassChange : 0,
      }
      
      //o.Insert(&cookie)

      this.SetSession("token", cookie.FormToken)
      this.SetSession(cookie.FormToken, cookie.CookieValue)
      this.SetSession("db_alias", this.Data["db_alias"].(string))

      data["token"] = cookie.FormToken      
    } else {
      data["Message"] = "Authentication Failed"  
    }    
  }
  
  return data
}

func is_valid_credentials(pass string, raw_pass string) bool {
  if pass == models.Encrypt_Pass(raw_pass) || (utf8.RuneCountInString(pass) < 40 && pass == raw_pass) {
    return true  
  }
  
  return false
}    
