package controllers

import (
  "github.com/astaxie/beego/orm"
  "github.com/astaxie/beego"
)

func IsTokenValid(o orm.Ormer, token string, secret string) bool {
  if beego.AppConfig.String("runmode") == "dev" {
    return true
  }
 
  qs := o.QueryTable("cookies").Filter("FormToken", token).Filter("CookieValue", secret)
  if exist := qs.Exist(); exist {
    return true    
  }
  
  return false  
}