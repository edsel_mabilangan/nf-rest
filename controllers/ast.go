package controllers

import (
  "bitbucket.org/edsel_mabilangan/nf-rest/models/amigo"
  "github.com/astaxie/beego"
  "github.com/astaxie/beego/orm"
  "fmt"
)

type AstActController struct {
  beego.Controller
}

func (this *AstActController) Post() {
  var data = make(map[string]string)
  data["result"] = "Failure"
  data["Message"] = "You have to login first"
   
  token := this.Input().Get("token")
  secret := this.Input().Get("secret")
  action := this.Ctx.Input.Param(":action")
  data["action"] = action
  
  if token != "" {
    this.Data["db_alias"] = this.GetSession("db_alias")
 
    o := orm.NewOrm()
    if this.Data["db_alias"] != nil {
      o.Using(this.Data["db_alias"].(string))
    }
    
    if IsTokenValid(o, token, secret) {
      ast_user, ast_pass, ast_ip, ast_port := GetAstCredentials()
      
      ami := AstConnect(ast_user, ast_pass, ast_ip, ast_port)
      if ami.Connected() {
        switch action {
          case "originate": {
            myPhone := this.Input().Get("myPhone")  
            targetPhone := this.Input().Get("targetPhone")
            
            if myPhone == targetPhone {
              data["Message"] = "Caller same as destination"
            } else {    
              ast_call := amigo.M {
                "Action":       "Originate",
                "Channel":      fmt.Sprintf("Local/%s@hud-caller-find-location/n", myPhone),
	            "MaxRetries":   "1",
	            "RetryTime":    "10",
	            "WaitTime":     "30",
	            "Callerid":     fmt.Sprintf("\"Test Caller\"<%s>", myPhone),
	            "Context":      "click-to-call",
	            "Exten":        targetPhone,
                "Priority":     "1",
	            "Variable":     fmt.Sprintf("CLICK_TO_CALL_EXTENSION=%s", myPhone),
              }  
              
              if result, err := ami.Action(ast_call); err == nil {  
                data["result"] = "Success"  
                data["Message"] = result["Message"]
              } else {
                data["Message"] = fmt.Sprintf("%#v", err)
              }
            }              
          }
          default:{
            data["Message"] = "Unsupported action in http request"
          }
        }
      } else {
        data["Message"] = "Not connected to Asterisk"  
      }
    }
  }
  
  this.Data["json"] = &data 
  this.ServeJson()
}

func DeviceStateChangeHandler (m amigo.M) {
    beego.Debug("DeviceStateChange event received", m)
}

func DefaultHandler (m amigo.M) {
    beego.Debug("Event received", m)
}

func AstConnect(username string, password string, ip string, port string) *amigo.Amigo {
  // Connect to Asterisk. Required arguments is username and password.
  a := amigo.New(username, password, ip, port)
  a.Connect()

  // Registering handler function for event "DeviceStateChange"
  a.RegisterHandler("DeviceStateChange", DeviceStateChangeHandler)

  // Registering default handler function for all events.
  a.RegisterDefaultHandler(DefaultHandler)

  // Optionally create channel to receiving all events
  // and set created channel to receive all events
  c := make(chan amigo.M)
  a.SetEventChannel(c)
  
  return a
}

func GetAstCredentials() (string, string, string, string) {
  if beego.AppConfig.String("runmode") == "dev" {
    return "remote_mgr", "E9r8UvGlyrOU8ZQrbPVI", "1.0.200.117", "5038"
  }
  
  return "","","",""
}